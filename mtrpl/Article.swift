//
//  Article.swift
//  testing
//
//  Created by fiveze on 18.02.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

import Foundation
import CoreData

@objc(Article)

class Article: NSManagedObject {
  
  @NSManaged var id: Int
  @NSManaged var about: String
  @NSManaged var blog: String
  @NSManaged var date: String
  @NSManaged var imageUrl: String
  @NSManaged var title: String
  @NSManaged var url: String
  @NSManaged var tags: NSSet
  @NSManaged var fave: Bool
  
}
