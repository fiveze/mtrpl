//
//  AppDelegate.swift
//  testing
//
//  Created by fiveze on 23.12.14.
//  Copyright (c) 2014 Sergey Tursunov. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  
  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    
    MagicalRecord.setupCoreDataStack()
    
    UINavigationBar.appearance().barTintColor = UIColor.blackColor()
    UINavigationBar.appearance().backgroundColor = UIColor.whiteColor()
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    
    UITabBar.appearance().tintColor = UIColor.blackColor();

    UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
    
    var mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
    var menu = mainStoryboard.instantiateViewControllerWithIdentifier("MenuViewController") as? MenuTableViewController
    SlideNavigationController.sharedInstance().leftMenu = menu
    SlideNavigationController.sharedInstance().menuRevealAnimationDuration = 0.18
    SlideNavigationController.sharedInstance().avoidSwitchingToSameClassViewController = false
    
    var button  = UIButton(frame: CGRectMake(0, 0, 30, 30))
    button.setImage(UIImage(named: "burger"), forState: .Normal)
    button.addTarget(SlideNavigationController.sharedInstance(), action: "toggleLeftMenu", forControlEvents: .TouchUpInside)
    var rightBarButtonItem = UIBarButtonItem(customView: button)
    SlideNavigationController.sharedInstance().leftBarButtonItem = rightBarButtonItem
    
    
    return true
  }
    
}

