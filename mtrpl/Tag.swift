//
//  Tag.swift
//  testing
//
//  Created by fiveze on 18.02.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

import Foundation
import CoreData

@objc(Tag)

class Tag: NSManagedObject {
  
  @NSManaged var name: String
  @NSManaged var article: Article
  
}
