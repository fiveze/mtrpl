//
//  MenuTableViewController.swift
//  mtrpl
//
//  Created by fiveze on 17.03.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
  
  var prevRow = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem()
  }
  
  override func viewWillAppear(animated: Bool) {
    self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), animated: false, scrollPosition: .Top)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    if(prevRow == indexPath.row) {
      SlideNavigationController.sharedInstance().closeMenuWithCompletion(nil)
      return
    } else {
      prevRow = indexPath.row
    }
    
    var mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
    
    switch indexPath.row {
    
    case 6:
      var viewController = mainStoryboard.instantiateViewControllerWithIdentifier("MagazineTableViewController") as? MagazineTableViewController
      
      SlideNavigationController.sharedInstance().popAllAndSwitchToViewController(viewController, withSlideOutAnimation:true,
        andCompletion:nil)
      
    default:
      var mainViewController = mainStoryboard.instantiateViewControllerWithIdentifier("MainViewController") as? MainViewController
      mainViewController?.blog = indexPath.row
      
      SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(mainViewController, withSlideOutAnimation:true,
        andCompletion:nil)
    }
    
  }
  
  override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    var bgColorView = UIView()
    bgColorView.backgroundColor = UIColor.redColor()
    cell.selectedBackgroundView = bgColorView
  }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using [segue destinationViewController].
  // Pass the selected object to the new view controller.
  }
  */
  
}
