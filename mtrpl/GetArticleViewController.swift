//
//  GetArticleViewController.swift
//  testing
//
//  Created by fiveze on 06.01.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

import UIKit
import WebKit

var myContext = 0

class GetArticleViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
  
  weak var webView: WKWebView?
  weak var progressView: WebViewProgressView?
  
  var article:Article!
  var stringUrlWeb:NSURLRequest?
  var favoriteData:NSMutableArray?
  
  @IBOutlet weak var favoriteButton: UIBarButtonItem!
  
  @IBAction func faveButtonClicked(sender: AnyObject) {
    
    if article.fave { // Deleting article from favorite list
      self.favoriteButton.image = UIImage(named: "favorite")
      
      article.fave = false
    
    } else { // Adding article to favorite list
      self.favoriteButton.image = UIImage(named: "favorite_add")
      
      article.fave = true
    }
    
    // Save the context.
    NSManagedObjectContext.contextForCurrentThread().saveToPersistentStoreAndWait()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationItem.title = article.title
    
    let url:NSURL = NSURL(string: article.url)!
    let request = NSURLRequest(URL: url)
    let webViewConfiguration = WKWebViewConfiguration()
    let webView = WKWebView(frame: self.view.bounds, configuration: webViewConfiguration)
    
    // WebView initializing
    webView.navigationDelegate = self
    webView.UIDelegate = self
    webView.allowsBackForwardNavigationGestures = true
    webView.addObserver(self, forKeyPath: "estimatedProgress", options: .New, context: &myContext)
    self.view.addSubview(webView)
    self.webView = webView
    
    // Add progress bar of loading
    let navigationBarBounds = self.navigationController?.navigationBar.bounds
    let progressView = WebViewProgressView(frame: CGRectMake(0, navigationBarBounds!.size.height - 2, navigationBarBounds!.size.width, 2))
    progressView.autoresizingMask = .FlexibleWidth | .FlexibleTopMargin
    self.navigationController?.navigationBar.addSubview(progressView)
    progressView.setProgress(0, animated: false)
    self.progressView = progressView
    
    webView.loadRequest(request)
    
    if article.fave == true {
      self.favoriteButton.image = UIImage(named: "favorite_add")
    }
  }
  
  deinit {
    // Delete progress bar of loading from superview
    self.webView?.removeObserver(self, forKeyPath: "estimatedProgress")
    self.progressView?.removeFromSuperview()
  }
  
  override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: ([NSObject : AnyObject]!), context: UnsafeMutablePointer<Void>) {
    
    if context != &myContext {
      super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
      return
    }
    
    if keyPath == "estimatedProgress" {
      if let progress: Float = change[NSKeyValueChangeNewKey]?.floatValue {
        self.progressView?.setProgress(progress, animated: true)
      }
      return
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let navigationBar = segue.destinationViewController as UINavigationController
    let destinationController = navigationBar.topViewController as WebViewController
    destinationController.urlGet = stringUrlWeb
    destinationController.progressViewWeb = progressView
  }
  
  // MARK: - WKNavigationDelegate methods
  
  func webView(webView: WKWebView!, decidePolicyForNavigationAction navigationAction: WKNavigationAction!, decisionHandler: ((WKNavigationActionPolicy) -> Void)!) {
    let url = navigationAction.request.URL
    
    switch navigationAction.navigationType {
    case .LinkActivated:
      self.stringUrlWeb = navigationAction.request
      self.performSegueWithIdentifier("showWebView", sender: self)
      
      decisionHandler(.Cancel)
      return
    default:
      break
    }
    
    decisionHandler(.Allow)
  }
  
  func webView(webView: WKWebView!, decidePolicyForNavigationResponse navigationResponse: WKNavigationResponse!, decisionHandler: ((WKNavigationResponsePolicy) -> Void)!) {
    
    decisionHandler(.Allow)
  }
  
}
