//
//  Main.swift
//  testing
//
//  Created by fiveze on 03.01.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

import UIKit

class MainLabel: UILabel {
  
  // Only override drawRect: if you perform custom drawing.
  // An empty implementation adversely affects performance during animation.
  override func drawRect(rect: CGRect) {
    // Drawing code
    let insets:UIEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets))
  }
  
  override func textRectForBounds(bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
    let insets:UIEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    var rect:CGRect = super.textRectForBounds(UIEdgeInsetsInsetRect(bounds, insets), limitedToNumberOfLines:numberOfLines)
    
    rect.origin.x    -= insets.left;
    rect.origin.y    -= insets.top;
    rect.size.width  += (insets.left + insets.right);
    rect.size.height += (insets.top + insets.bottom);
    
    return rect;
  }
  
}
