//
//  Seller.swift
//  mtrpl
//
//  Created by fiveze on 19.03.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

import Foundation

struct Seller {
  var name:String
  var description:String
}

var magazines = [
  "Москва": [
    Seller(name: "Веранда 32.05", description: "Каретный Ряд, 3, стр. 6"),
    Seller(name: "Кинотеатр «35мм»", description: "ул. Покровка, 47/24"),
    Seller(name: "Книжный магазин ArtBookShop", description: "ул. Петровка, 25, стр.1"),
    Seller(name: "Книжный магазин ArtBookShop", description: "Гоголевский б-р, 10"),
    Seller(name: "Книжный магазин ArtBookShop", description: "Берсеневская наб., 14, стр. 5а, институт Strelka"),
    Seller(name: "Книжный магазин ArtBookShop", description: "Крымский Вал, 9, Музей современного искусства «Гараж», павильон у Пионерского пруда в Парке Горького"),
    Seller(name: "Магазин BagBox", description: "1-й Кожуховский пр-д, 11"),
    Seller(name: "Винный бар Brix", description: "Малый Козихинский пер., 10/1"),
    Seller(name: "Винный бар Brix II", description: "ул. Пятницкая, 71/5, стр. 2"),
    Seller(name: "Школа телевидения Cinemotion", description: "Бол. Знаменский пер., 2, стр. 3"),
    Seller(name: "Ресторан Coin", description: "ул. Пятницкая, 71/5, стр. 2"),
    Seller(name: "Кафе Crabs are coming", description: "ул. Бол. Никитская, 23/14/9"),
    Seller(name: "Кафе Dada", description: "1-й Новокузнецкий пер., 5/7"),
    Seller(name: "Ресторан Delicatessen", description: "ул. Садовая-Каретная, 20, стр. 2"),
    Seller(name: "Dewar’s Powerhouse", description: "ул. Гончарная, 7/4"),
    Seller(name: "DooDoo Studio", description: "Олимпийский пр-т, 16, к. 1"),
    Seller(name: "DooDoo Studio", description: "СК «Олимпийский», под. 9а, 7 эт."),
    Seller(name: "Магазин Feelosophy", description: "Б. Спасоглинищевский пер., 9/1, стр. 10"),
    Seller(name: "Магазин одежды Free Flow", description: "ул. Пятницкая, 3/4, стр. 2"),
    Seller(name: "Good Local", description: "ул. Б. Новодмитровская, 36 с.9"),
    Seller(name: "Good Local", description: "Таганская пл., 86"),
    Seller(name: "Книжный магазин LibroRoom", description: "Ленинградский пр-т, 80, корп. 21"),
    Seller(name: "Книжный магазин LibroRoom", description: "Балтийская, 9"),
    Seller(name: "Магазин Limited Edition", description: "ул. Большая Семеновская, 32, 2 эт., оф. 200"),
    Seller(name: "Тату-салон Love Life Tattoo", description: "Бобров пер., 2"),
    Seller(name: "Тату-салон Maverick Custom Tattoo & Barber Shop", description: "ул. Большая Ордынка, 17"),
    Seller(name: "Moscow Coding School", description: "ул. Тверская, 7"),
    Seller(name: "Парикмахерская Noir ", description: "Старопименовский пер., 4, стр.1"),
    Seller(name: "Барбершоп Schegol", description: "Малый Кисловский пер., 9, стр. 1"),
    Seller(name: "Клуб Rodnya", description: "ул. Нижн. Сыромятническая, 10, стр. 7"),
    Seller(name: "Sock Club Moscow", description: ""),
    Seller(name: "PUNTO", description: "«Красный Октябрь», Берсеневский пер., 2, стр. 1"),
    Seller(name: "Ботанический сад МГУ «Аптекарский огород»", description: "пр-т Мира 26, стр. 1 (главный вход в Ботанический сад МГУ «Аптекарский огород»)"),
    Seller(name: "Британская высшая школа дизайна", description: "ул. Нижняя Сыромятническая, 10,  стр. 3, центр дизайна Artplay"),
    Seller(name: "Книжный магазин Гоголь Books", description: "ул. Казакова, 8, фойе 1-го этажа Гоголь-центра"),
    Seller(name: "Государственный центр современного искусства (ГЦСИ)", description: "ул. Зоологическая, 13, стр. 2"),
    Seller(name: "Культурный центр ЗИЛ", description: "ул. Восточная, 4, к. 1."),
    Seller(name: "Кинотеатр «Иллюзион»", description: "Котельническая наб., 1/15"),
    Seller(name: "Центр фотографии им. братьев Люмьер", description: "Болотная наб., 3, стр. 1"),
    Seller(name: "Магазин «Мир кино»", description: "ул. Маросейка, 6/8"),
    Seller(name: "Торговый дом книги «Москва»", description: "ул. Тверская, 8/2, стр.1"),
    Seller(name: "Торговый дом книги «Москва»", description: "ул. Воздвиженка, 4/7, стр.1"),
    Seller(name: "Кинотеатр «Пионер»", description: "Кутузовский пр-т, 21"),
    Seller(name: "Магазин и тату-мастерская «Поколение»", description: "4-й Сыромятнический пер., 3/5, стр. 4а"),
    Seller(name: "Культурный Центр «Пунктум»", description: "ул. Тверская, 12, стр. 2, 4 эт."),
    Seller(name: "￼Магазин «Просто так»", description: "￼ул. Забелина, 3, стр. 7"),
    Seller(name: "￼Магазин «Просто так»", description: "￼Малый Гнездниковский переулок, 12/27"),
    Seller(name: "Книжный магазин «Республика»", description: "ул. 1-я Тверская-Ямская, 10"),
    Seller(name: "Книжный магазин «Республика»", description: "Цветной б-р, 15, стр. 1 (универмаг «Цветной», первый этаж)"),
    Seller(name: "Книжный магазин «Республика»", description: "ул. Большая Новодмитровская, 36 (дизайн-завод Flacon)"),
    Seller(name: "Книжный магазин «Республика»", description: "ул. Новый Арбат, 19"),
    Seller(name: "Книжный магазин «Республика»", description: "ул. Мясницкая, 24/7, стр. 1"),
    Seller(name: "Книжный магазин «Республика»", description: "ул. Большая Тульская, 13 (ТРЦ «Ереван Плаза»)"),
    Seller(name: "Российская государственная бибилиотека для молодежи", description: "Большая Черкизовская ул., 4, корп. 1"),
    Seller(name: "СУП-кафе", description: "1-я Брестская, 62"),
    Seller(name: "Книжный магазин «Ходасевич»", description: "уул. Покровка, 6"),
    Seller(name: "Кафе «Цайт»", description: "Даев пер., 33"),
    Seller(name: "Свободное пространство «Циферблат»", description: "ул. Покровка, 12, стр. 1"),
    Seller(name: "Свободное пространство «Циферблат»", description: "ул. Тверская, 12, стр.1, оф. 11"),
    Seller(name: "Кафе «ЦурЦум»", description: "4-й Сыромятнический пер., 1/8, стр.6, центр современного искусства «Винзавод»")
  ],
  "Санкт-Петербург": [
    Seller(name: "Галерея современного искусства Anna Nova", description: "ул. Жуковского, 28"),
    Seller(name: "Магазин BagBox", description: "Большой пр-т П. С., 100"),
    Seller(name: "Бар Brimborium", description: "ул. Маяковского, 22–24"),
    Seller(name: "Wood Bar", description: "ул. Марата, 34"),
    Seller(name: "Good Local", description: "Наб. Обводного канала, 60"),
    Seller(name: "Library Bar", description: "В.О, Средний пр., 5"),
    Seller(name: "Mitte Cafe", description: "ул. Рубинштейна, 27"),
    Seller(name: "Кафе Umao", description: "Конногвардейский б-р, 11."),
    Seller(name: "DooDooStudio Spb", description: "ул. 7-я Красноармейская, 9 (вход во двор)"),
    Seller(name: "Магазин «Подписные издания»", description: "Литейный пр-т, 57"),
    Seller(name: "Магазин Room Store", description: "наб. канала Грибоедова, 26, эт. 3")
  ],
  "Астрахань": [
    Seller(name: "Jab Barbershop", description: "Наб. 1 Мая, 91 / ул. Мечникова, 9 (угол дома)")
  ],
  "Белгород": [
    Seller(name: "Кофейная компания «Калипсо»", description: "Пр-т. Богдана Хмельницкого, 79")
  ],
  "Владивосток": [
    Seller(name: "Geometria.ru", description: ""),
    Seller(name: "Рюкзак-шоп «Сопка»", description: "ул. Адмирала Фокина, 5"),
    Seller(name: "Студия «Неформат»", description: "ул. Алеутская, 11, здание «Приморгражданпроекта», 10-й эт., оф. 1001/1")
  ],
  "Воронеж": [
    Seller(name: "Магазин-клуб FreakFabrique", description: "ул. Никитинская, 2 (перекресток с Комиссаржевской)")
  ],
  "Екатеринбург": [
    Seller(name: "Книжный магазин «Йозеф Кнехт»", description: "ул. 8 Марта, 7")
  ],
  "Ижевск": [
    Seller(name: "Арт-пространство «Сахар»", description: "ул. Горького, 76, 2 эт. (центр напротив Национального театра и Летнего сада)")
  ],
  "Иркутск": [
    Seller(name: "Кафе NUUT", description: "ул. Сухэ-Батора, 10"),
    Seller(name: "Арт-пространство «Магазин подарков №1»", description: "")
  ],
  "Казань": [
    Seller(name: "Парикмахерская Chop-Chop", description: "ул. Профсоюзная, 1")
  ],
  "Краснодар": [
    Seller(name: "Шоурум JIGO", description: "ул. Мира, 44")
  ],
  "Красноярск": [
    Seller(name: "Парикмахерская Chop­-chop", description: "ул. Красной Армии, 9/1")
  ],
  "Липецк": [
    Seller(name: "Rob Roy Bar", description: "ул. Ворошилова, 1")
  ],
  "Мурманск": [
    Seller(name: "New York Coffee (Тайм-Кофейня)", description: "ул. Буркова, 13")
  ],
  "Новороссийск": [
    Seller(name: "￼Парикмахерская Chop­-сhop", description: "ул. Мира, 35")
  ],
  "Новосибирск": [
    Seller(name: "￼￼Сеть суши­-баров «Рыба.Рис»", description: "ул. Ленина, 1"),
    Seller(name: "￼￼Сеть суши­-баров «Рыба.Рис»", description: "￼￼￼￼пл. Калинина, Красный пр-т, 186/1 ￼￼"),
    Seller(name: "￼￼Сеть суши­-баров «Рыба.Рис»", description: "￼ТПЦ »Сибирский Молл», ул. Фрунзе, 238 (1-й этаж)"),
    Seller(name: "￼￼Сеть суши­-баров «Рыба.Рис»", description: "￼ТРЦ «Аура», ул. Военная, 5 (4-й этаж)￼￼￼￼￼￼￼"),
    Seller(name: "￼￼Сеть суши­-баров «Рыба.Рис»", description: "￼ТРК «Калина-центр», ул. Дуси Ковальчук, 179/4 (3-й этаж)")
  ],
  "Омск": [
    Seller(name: "Парикмахерская Chop-Chop", description: "ул. Орджоникидзе, 16")
  ],
  "Ростов-на-Дону": [
    Seller(name: "«Киоск»", description: "￼ул. Суворова, 52а")
  ],
  "Саратов": [
    Seller(name: "Кофейня Love-Coffee", description: "ул. Московская, 157")
  ],
  "Смоленск": [
    Seller(name: "Blackberry Hookah Bar", description: "ул. Пржевальского, 2")
  ],
  "Тула": [
    Seller(name: "ПарикмахерскаяChop-Chop", description: "Учетный пер., 2")
  ],
  "Тюмень": [
    Seller(name: "Книжный магазин «Перспектива»", description: "ул. Челюскинцев, 36"),
    Seller(name: "Dock Store", description: "ул. Максима Горького, 68/7")
  ],
  "Ульяновск": [
    Seller(name: "Парикмахерская Chop­-сhop", description: "ул. Ленина, 89")
  ],
  "Уфа": [
    Seller(name: "￼Парикмахерская Chop­-сhop", description: "ул. Карла Маркса, 33")
  ],
  "Челябинск": [
    Seller(name: "Интернет-издание Feelmore.ru", description: "ул. Клары Цеткин, 11, корп. 2")
  ]
]