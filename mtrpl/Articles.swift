//
//  Articles.swift
//  testing
//
//  Created by fiveze on 28.12.14.
//  Copyright (c) 2014 Sergey Tursunov. All rights reserved.
//

import Foundation

let TopAppURL = "http://borzenko.com/tursunov"

class Articles {
  class func getList(success: ((articlesData: NSData!) -> Void)) {
    loadDataFromURL(NSURL(string: TopAppURL)!, completion:{(data, error) -> Void in
      
      if let urlData = data {
        success(articlesData: urlData)
      }
      if let urlError = error {
        println(urlError)
      }
    })
  }
  
  class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
    var session = NSURLSession.sharedSession()
    
    // Use NSURLSession to get data from an NSURL
    let loadDataTask = session.dataTaskWithURL(url, completionHandler: { (data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
      if let responseError = error {
        completion(data: nil, error: responseError)
      } else if let httpResponse = response as? NSHTTPURLResponse {
        if httpResponse.statusCode != 200 {
          var statusError = NSError(domain:"com.tursunov", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
          completion(data: nil, error: statusError)
        } else {
          completion(data: data, error: nil)
        }
      }
    })
    
    loadDataTask.resume()
  }
  
}