//
//  MainTableViewCell.swift
//  testing
//
//  Created by fiveze on 26.12.14.
//  Copyright (c) 2014 Sergey Tursunov. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
  
  
  @IBOutlet weak var articleImage: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var blogLabel: UILabel!
  @IBOutlet weak var firstTagLabel: MainLabel!
  @IBOutlet weak var secondTagLabel: MainLabel!
  @IBOutlet weak var thirdTagLabel: MainLabel!
  
  var topSpace:CGFloat!
  
  @IBOutlet weak var topSpaceConstraint: NSLayoutConstraint!
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    self.contentView.layoutIfNeeded()
    self.titleLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.titleLabel.frame)
    self.descriptionLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.descriptionLabel.frame)
  }
  
}
