//
//  MainTableViewController.swift
//  testing
//
//  Created by fiveze on 26.12.14.
//  Copyright (c) 2014 Sergey Tursunov. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class MainViewController: UITableViewController, NSFetchedResultsControllerDelegate {
  var imageCache = [String : UIImage]()
  var articles:[Article]!
  var blog = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.fetchData()
    
    var refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: Selector("refresh"), forControlEvents: UIControlEvents.ValueChanged)
    self.refreshControl = refreshControl
    self.refreshControl?.beginRefreshing()
    self.refresh()
    
    tableView.estimatedRowHeight = 421.0
    tableView.rowHeight = UITableViewAutomaticDimension
  }
  
  func fetchData() {
    switch blog {
    case 1:
      self.navigationItem.title = "Боги"
      
      let predicate = NSPredicate(format: "blog = %@", "Боги")
      articles = Article.findAllSortedBy("id", ascending:false, withPredicate: predicate) as [Article]
    case 2:
      self.navigationItem.title = "Война"
      
      let predicate = NSPredicate(format: "blog = %@", "Война")
      articles = Article.findAllSortedBy("id", ascending:false, withPredicate: predicate) as [Article]
    case 3:
      self.navigationItem.title = "Секс"
      
      let predicate = NSPredicate(format: "blog = %@", "Секс")
      articles = Article.findAllSortedBy("id", ascending:false, withPredicate: predicate) as [Article]
    case 4:
      self.navigationItem.title = "Боль"
      
      let predicate = NSPredicate(format: "blog = %@", "Боль")
      articles = Article.findAllSortedBy("id", ascending:false, withPredicate: predicate) as [Article]
    case 5:
      self.navigationItem.title = "Визии"
      
      let predicate = NSPredicate(format: "blog = %@", "Визии")
      articles = Article.findAllSortedBy("id", ascending:false, withPredicate: predicate) as [Article]
    case 7:
      self.navigationItem.title = "Избранное"
      
      let predicate = NSPredicate(format: "fave = %@", true)
      articles = Article.findAllSortedBy("id", ascending:false, withPredicate: predicate) as [Article]
    default:
      self.navigationItem.title = "Метрополь"
      
      articles = Article.findAllSortedBy("id", ascending:false) as [Article]
    }
  }
  
  func slideNavigationControllerShouldDisplayLeftMenu()->Bool
  {
    return true
  }
  
  func refresh() {
    Articles.getList { (articleData) -> Void in // Get articles list
      let json = JSON(data: articleData)
      
      for (key: String, articleDict: JSON) in json {
        let predicate = NSPredicate(format: "id = %@", articleDict["id"].stringValue)
        
        var count = Article.numberOfEntitiesWithPredicate(predicate)
        
        if count == 0 {
          var articleEntity = Article.createEntity() as Article
          
          articleEntity.id = articleDict["id"].intValue
          articleEntity.blog = articleDict["blog"].stringValue
          articleEntity.date = articleDict["date"].stringValue
          articleEntity.about = articleDict["about"].stringValue
          articleEntity.imageUrl = articleDict["image"].stringValue
          articleEntity.title = articleDict["title"].stringValue
          articleEntity.url = articleDict["url"].stringValue
          articleEntity.fave = false
          
          articleEntity.tags = NSMutableSet()
          
          for (key: String, tagName: JSON) in articleDict["tags"] {
            var tagEntity = Tag.createEntity() as Tag
            tagEntity.name = tagName.stringValue
            
            tagEntity.article = articleEntity
          }
          
          // Save the context.
          NSManagedObjectContext.contextForCurrentThread().saveToPersistentStoreAndWait()
          //self.articles.insert(articleEntity, atIndex: key.toInt()!)
          self.fetchData()
        } else {
            break;
        }
      }
      
      dispatch_sync(dispatch_get_main_queue()) {
        self.tableView.reloadData()
      }
      
      self.refreshControl?.endRefreshing()
    }
    
  }
 
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return articles.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("mainCell", forIndexPath: indexPath) as MainTableViewCell
    let article = articles[indexPath.row]
    
    cell.titleLabel.text = article.title
    cell.descriptionLabel.text = article.about
    cell.dateLabel.text = article.date
    cell.blogLabel.text = article.blog.uppercaseString
    
    cell.firstTagLabel.hidden = true
    cell.secondTagLabel.hidden = true
    cell.thirdTagLabel.hidden = true
    
    
    var tags = article.tags.allObjects as [Tag]
    
    for (i, tag) in enumerate(tags) {
      if i == 0 {
        cell.firstTagLabel.text = tags[0].name.uppercaseString
        cell.firstTagLabel.hidden = false
        cell.firstTagLabel.layer.borderColor = UIColor.blackColor().CGColor
        cell.firstTagLabel.layer.borderWidth = 1.0
      } else if i == 1 {
        cell.secondTagLabel.text = tags[1].name.uppercaseString
        cell.secondTagLabel.hidden = false
        cell.secondTagLabel.layer.borderColor = UIColor.blackColor().CGColor
        cell.secondTagLabel.layer.borderWidth = 1.0
      } else if i == 2 {
        cell.thirdTagLabel.text = tags[2].name.uppercaseString
        cell.thirdTagLabel.hidden = false
        cell.thirdTagLabel.layer.borderColor = UIColor.blackColor().CGColor
        cell.thirdTagLabel.layer.borderWidth = 1.0
      }
    }
    
    let urlStringImage = article.imageUrl.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
    var imageUrl: NSURL = NSURL(string: urlStringImage)!
    
    cell.articleImage.setImageWithURL(imageUrl, usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    
    
    return cell
  }
  
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "showArticle" {
      if let indexPath = self.tableView.indexPathForSelectedRow() {
        (segue.destinationViewController as GetArticleViewController).article = articles[indexPath.row]
      }
    }
  }
}
