//
//  WebViewController.swift
//  WebKitDemo
//
//  Created by Kyusaku Mihara on 9/17/14.
//  Copyright (c) 2014 epohsoft. All rights reserved.
//

import UIKit
import WebKit

var myContextWeb = 0

class WebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
  
  weak var webViewWeb: WKWebView?
  weak var progressViewWeb: WebViewProgressView!
  @IBOutlet weak var backBarButton: UIBarButtonItem!
  @IBOutlet weak var forwardBarButton: UIBarButtonItem!
  @IBOutlet weak var stopBarButton: UIBarButtonItem!
  @IBOutlet weak var refreshBarButton: UIBarButtonItem!
  
  @IBAction func cancelTapped(segue: UIStoryboardSegue) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  
  var urlGet:NSURLRequest!
  
  deinit {
    self.webViewWeb?.removeObserver(self, forKeyPath: "loading")
    self.webViewWeb?.removeObserver(self, forKeyPath: "title")
    self.webViewWeb?.removeObserver(self, forKeyPath: "estimatedProgress")
    self.webViewWeb?.removeObserver(self, forKeyPath: "canGoBack")
    self.webViewWeb?.removeObserver(self, forKeyPath: "canGoForward")
    self.progressViewWeb?.removeFromSuperview()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let webViewWebConfiguration = WKWebViewConfiguration()
    
    let webViewWeb = WKWebView(frame: self.view.bounds, configuration: webViewWebConfiguration)
    
    webViewWeb.navigationDelegate = self
    webViewWeb.UIDelegate = self
    webViewWeb.allowsBackForwardNavigationGestures = true
    webViewWeb.autoresizingMask = .FlexibleWidth | .FlexibleHeight
    
    webViewWeb.addObserver(self, forKeyPath: "loading", options: .New, context: &myContextWeb)
    webViewWeb.addObserver(self, forKeyPath: "title", options: .New, context: &myContextWeb)
    webViewWeb.addObserver(self, forKeyPath: "estimatedProgress", options: .New, context: &myContextWeb)
    webViewWeb.addObserver(self, forKeyPath: "canGoBack", options: .New, context: &myContextWeb)
    webViewWeb.addObserver(self, forKeyPath: "canGoForward", options: .New, context: &myContextWeb)
    
    self.view.addSubview(webViewWeb)
    self.webViewWeb = webViewWeb
    
    let navigationBarBounds = self.navigationController?.navigationBar.bounds
    let progressViewWeb = WebViewProgressView(frame: CGRectMake(0, navigationBarBounds!.size.height - 2, navigationBarBounds!.size.width, 2))
    progressViewWeb.autoresizingMask = .FlexibleWidth | .FlexibleTopMargin
    self.navigationController?.navigationBar.addSubview(progressViewWeb)
    self.progressViewWeb = progressViewWeb
    
    webViewWeb.loadRequest(urlGet)
    
    self.backBarButton.enabled = false
    self.forwardBarButton.enabled = false
    
  }
  
  override func observeValueForKeyPath(keyPath: String, ofObject object: (AnyObject!), change: ([NSObject : AnyObject]!), context: UnsafeMutablePointer<Void>) {
    if context != &myContextWeb {
      super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
      return
    }
    
    if keyPath == "loading" {
      if let loading = change[NSKeyValueChangeNewKey]?.boolValue {
        self.stopBarButton?.enabled = loading
        self.refreshBarButton?.enabled = !loading
      }
      return
    }
    
    if keyPath == "title" {
      self.navigationItem.title = change[NSKeyValueChangeNewKey]? as NSString
      return
    }
    
    if keyPath == "estimatedProgress" {
      if let progress: Float = change[NSKeyValueChangeNewKey]?.floatValue {
        self.progressViewWeb?.setProgress(progress, animated: true)
      }
      return
    }
    
    if keyPath == "canGoBack" {
      if let canGoBack = change[NSKeyValueChangeNewKey]?.boolValue {
        self.backBarButton.enabled = canGoBack
      }
      return
    }
    
    if keyPath == "canGoForward" {
      if let canGoForward = change[NSKeyValueChangeNewKey]?.boolValue {
        self.forwardBarButton.enabled = canGoForward
      }
      return
    }
  }
  
  @IBAction func backBarButtonTapped(sender: AnyObject) {
    if self.webViewWeb!.canGoBack {
      self.webViewWeb!.goBack()
    }
  }
  
  @IBAction func forwardBarButtonTapped(sender: AnyObject) {
    if self.webViewWeb!.canGoForward {
      self.webViewWeb!.goForward()
    }
  }
  
  @IBAction func stopBarButtonTapped(sender: AnyObject) {
    if self.webViewWeb!.loading {
      self.webViewWeb!.stopLoading()
    }
  }
  
  @IBAction func refreshBarButtonTapped(sender: AnyObject) {
    self.webViewWeb?.reload()
  }
  
  // MARK: - WKNavigationDelegate methods
  
  func webView(webView: WKWebView!, didStartProvisionalNavigation navigation: WKNavigation!) {
    println("webView:\(webView) didStartProvisionalNavigation:\(navigation)")
  }
  
  func webView(webView: WKWebView!, didCommitNavigation navigation: WKNavigation!) {
    println("webView:\(webView) didCommitNavigation:\(navigation)")
  }
  
  func webView(webView: WKWebView!, decidePolicyForNavigationAction navigationAction: WKNavigationAction!, decisionHandler: ((WKNavigationActionPolicy) -> Void)!) {
    println("webView:\(webView) decidePolicyForNavigationAction:\(navigationAction) decisionHandler:\(decisionHandler)")
    
    let url = navigationAction.request.URL
    
    switch navigationAction.navigationType {
    case .LinkActivated:
      if navigationAction.targetFrame == nil {
        self.webViewWeb?.loadRequest(navigationAction.request)
      }
    default:
      break
    }
    
    decisionHandler(.Allow)
  }
  
  func webView(webView: WKWebView!, decidePolicyForNavigationResponse navigationResponse: WKNavigationResponse!, decisionHandler: ((WKNavigationResponsePolicy) -> Void)!) {
    println("webView:\(webView) decidePolicyForNavigationResponse:\(navigationResponse) decisionHandler:\(decisionHandler)")
    
    decisionHandler(.Allow)
  }
  
  func webView(webView: WKWebView!, didReceiveAuthenticationChallenge challenge: NSURLAuthenticationChallenge!, completionHandler: ((NSURLSessionAuthChallengeDisposition, NSURLCredential!) -> Void)!) {
    println("webView:\(webView) didReceiveAuthenticationChallenge:\(challenge) completionHandler:\(completionHandler)")
    
    let alertController = UIAlertController(title: "Authentication Required", message: ":(", preferredStyle: .Alert)
    weak var usernameTextField: UITextField!
    alertController.addTextFieldWithConfigurationHandler { textField in
      textField.placeholder = "Username"
      usernameTextField = textField
    }
    weak var passwordTextField: UITextField!
    alertController.addTextFieldWithConfigurationHandler { textField in
      textField.placeholder = "Password"
      textField.secureTextEntry = true
      passwordTextField = textField
    }
    alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
      completionHandler(.CancelAuthenticationChallenge, nil)
    }))
    alertController.addAction(UIAlertAction(title: "Log In", style: .Default, handler: { action in
      let credential = NSURLCredential(user: usernameTextField.text, password: passwordTextField.text, persistence: NSURLCredentialPersistence.ForSession)
      completionHandler(.UseCredential, credential)
    }))
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func webView(webView: WKWebView!, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
    println("webView:\(webView) didReceiveServerRedirectForProvisionalNavigation:\(navigation)")
  }
  
  func webView(webView: WKWebView!, didFinishNavigation navigation: WKNavigation!) {
    println("webView:\(webView) didFinishNavigation:\(navigation)")
  }
  
  func webView(webView: WKWebView!, didFailNavigation navigation: WKNavigation!, withError error: NSError!) {
    println("webView:\(webView) didFailNavigation:\(navigation) withError:\(error)")
  }
  
  func webView(webView: WKWebView!, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError!) {
    println("webView:\(webView) didFailProvisionalNavigation:\(navigation) withError:\(error)")
  }
  
  // MARK: WKUIDelegate methods
  
  func webView(webView: WKWebView!, runJavaScriptAlertPanelWithMessage message: String!, initiatedByFrame frame: WKFrameInfo!, completionHandler: (() -> Void)!) {
    println("webView:\(webView) runJavaScriptAlertPanelWithMessage:\(message) initiatedByFrame:\(frame) completionHandler:\(completionHandler)")
    
    let alertController = UIAlertController(title: frame.request.URL.host, message: message, preferredStyle: .Alert)
    alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
      completionHandler()
    }))
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func webView(webView: WKWebView!, runJavaScriptConfirmPanelWithMessage message: String!, initiatedByFrame frame: WKFrameInfo!, completionHandler: ((Bool) -> Void)!) {
    println("webView:\(webView) runJavaScriptConfirmPanelWithMessage:\(message) initiatedByFrame:\(frame) completionHandler:\(completionHandler)")
    
    let alertController = UIAlertController(title: frame.request.URL.host, message: message, preferredStyle: .Alert)
    alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
      completionHandler(false)
    }))
    alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
      completionHandler(true)
    }))
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func webView(webView: WKWebView!, runJavaScriptTextInputPanelWithPrompt prompt: String!, defaultText: String!, initiatedByFrame frame: WKFrameInfo!, completionHandler: ((String!) -> Void)!) {
    println("webView:\(webView) runJavaScriptTextInputPanelWithPrompt:\(prompt) defaultText:\(defaultText) initiatedByFrame:\(frame) completionHandler:\(completionHandler)")
    
    let alertController = UIAlertController(title: frame.request.URL.host, message: prompt, preferredStyle: .Alert)
    weak var alertTextField: UITextField!
    alertController.addTextFieldWithConfigurationHandler { textField in
      textField.text = defaultText
      alertTextField = textField
    }
    alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
      completionHandler(nil)
    }))
    alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
      completionHandler(alertTextField.text)
    }))
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
}
