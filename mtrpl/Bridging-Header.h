//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#define MR_SHORTHAND 1
#import <Foundation/Foundation.h>
#import "CoreData+MagicalRecord.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "SlideNavigationController.h"